﻿using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Interfaces;
using MartianRobots.Domain.Contracts.Models;
using MartianRobots.Domain.Services.Contexts;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MartianRobots.Domain.Services
{
    public class MartianRobotsService : IMartianRobotsService
    {
        protected readonly MartianRobotsServiceContext _context;

        public MartianRobotsService() 
        {
            _context = new MartianRobotsServiceContext();
        }

        public void InitializeContext(int xMaxCoordinate, int yMaxCoordinate, IEnumerable<RobotModel> robots)
        {
            if (_context.Surface != null)
                throw new Exception("Context is already initialized");

            _context.InitializeSurface(xMaxCoordinate, yMaxCoordinate);
            _context.InitializeRobots(robots);
        }

        public void MoveRobot(int id, RobotCommandEnum command)
        {
            if (_context == null)
                throw new Exception("Сontext needs to be initialized");

            var robot = _context.Robots.FirstOrDefault(r => r.Id == id);
            if (robot == null)
                throw new ArgumentException($"Robot with Id={id} does not exists");

            if (robot.IsLost)
                return;

            var currentPosition = robot.Position;
            var currentOrientation = robot.Orientation;
            
            (var calculatedPosition, var calculatedOrientation) = CalculateMovement(robot, command);

            if (CheckIsLost(calculatedPosition) && _context.Surface.RobotScentPoints.Contains(currentPosition))
                return;

            if (CheckIsLost(calculatedPosition))
            {
                _context.Surface.RobotScentPoints.Add(currentPosition);
                robot.IsLost = true;
                return;
            }

            DoMovement(robot, command);
        }

        private (SurfacePointModel, RobotOrientationEnum) CalculateMovement(RobotModel robot, RobotCommandEnum command)
        {
            var position = new SurfacePointModel(robot.Position);
            var orientation = robot.Orientation;

            switch (command)
            {
                case RobotCommandEnum.Left:
                    orientation = robot.CalculateRotateLeft();
                    break;
                case RobotCommandEnum.Right:
                    orientation = robot.CalculateRotateRight();
                    break;
                case RobotCommandEnum.Forward:
                    position = robot.CalculateMoveForward();
                    break;
                default:
                    throw new Exception("Unknown robot command");
            }

            return (position, orientation);
        }

        private void DoMovement(RobotModel robot, RobotCommandEnum command)
        {
            switch (command)
            {
                case RobotCommandEnum.Left:
                    robot.RotateLeft();
                    break;
                case RobotCommandEnum.Right:
                    robot.RotateRight();
                    break;
                case RobotCommandEnum.Forward:
                    robot.MoveForward();
                    break;
                default:
                    throw new Exception("Unknown robot command");
            }
        }

        private bool CheckIsLost(SurfacePointModel point)
        {
            return point.XCoordinate > _context.Surface.XMaxCoordinate
                || point.YCoordinate > _context.Surface.YMaxCoordinate
                || point.YCoordinate < _context.Surface.XMinCoordinate
                || point.YCoordinate < _context.Surface.YMinCoordinate;
        }

        public RobotModel GetRobot(int id)
        {
            if (_context == null)
                throw new Exception("Сontext needs to be initialized");

            return _context.Robots.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<RobotModel> GetRobots()
        {
            if (_context == null)
                throw new Exception("Сontext needs to be initialized");

            return _context.Robots;
        }
    }
}