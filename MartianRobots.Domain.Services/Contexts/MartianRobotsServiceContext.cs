﻿using MartianRobots.Domain.Contracts.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MartianRobots.Domain.Services.Contexts
{
    public class MartianRobotsServiceContext
    {
        public SurfaceModel Surface { get; private set; }

        public IEnumerable<RobotModel> Robots { get; private set; }

        public MartianRobotsServiceContext()
        {
            Robots = new List<RobotModel>();
        }

        public void InitializeSurface(int xMaxCoordinate, int yMaxCoordinate)
        {
            Surface = Surface == null
                ? new SurfaceModel(xMaxCoordinate, yMaxCoordinate)
                : throw new Exception("Failed to initilize: Surface is already initialized");
        }

        public void InitializeRobots(IEnumerable<RobotModel> robots)
        {
            if (robots == null)
                throw new ArgumentNullException("Failed to initilize: Robots collection is null");

            if (robots.GroupBy(r => r.Id).Any(g => g.Count() > 1))
                throw new ArgumentException("Failed to initilize: Robots collection has duplicates");

            Robots = !Robots.Any()
                ? robots
                : throw new Exception("Failed to initilize: Robots are already initialized");
        }
    }
}