﻿using MartianRobots.Application.Infrastructure.ConfigExtensions;

using Microsoft.Extensions.DependencyInjection;

using System;

namespace MartianRobots.Application.Infrastructure
{
    public class Bootstrapper
    {
        protected readonly IServiceCollection _services;

        public Bootstrapper()
        {
            _services = new ServiceCollection();
        }

        public void ConfigureServices()
        {
            _services
                .AddConfiguredAutoMapper()
                .AddHandlers()
                .AddDomainServices();
        }

        public IServiceProvider Build()
        {
            return _services.BuildServiceProvider();
        }
    }
}