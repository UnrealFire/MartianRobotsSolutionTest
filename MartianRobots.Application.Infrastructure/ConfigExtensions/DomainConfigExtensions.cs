﻿using MartianRobots.Domain.Contracts.Interfaces;
using MartianRobots.Domain.Services;

using Microsoft.Extensions.DependencyInjection;

namespace MartianRobots.Application.Infrastructure.ConfigExtensions
{
    internal static class DomainConfigExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<IMartianRobotsService, MartianRobotsService>();

            return services;
        }
    }
}