﻿using MartianRobots.Application.Mapping;
using MartianRobots.Domain.Mapping;

using Microsoft.Extensions.DependencyInjection;

namespace MartianRobots.Application.Infrastructure.ConfigExtensions
{
    internal static class AutoMapperConfigExtension
    {
        public static IServiceCollection AddConfiguredAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg => cfg.ShouldUseConstructor = ci => ci.IsPrivate, typeof(ApplicationMappingMarkerType), typeof(DomainMappingMarkerType));

            return services;
        }
    }
}