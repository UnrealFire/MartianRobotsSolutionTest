﻿using MartianRobots.Application.Handlers.Command;

using Microsoft.Extensions.DependencyInjection;

namespace MartianRobots.Application.Infrastructure.ConfigExtensions
{
    internal static class ApplicationConfigExtension
    {
        public static IServiceCollection AddHandlers(this IServiceCollection services)
        {
            services.AddScoped<MartianRobotsCommandHandler>();

            return services;
        }
    }
}