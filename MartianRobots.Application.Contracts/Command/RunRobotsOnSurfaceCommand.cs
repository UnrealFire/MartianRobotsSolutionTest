﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MartianRobots.Application.Contracts.Command
{
    public class RunRobotsOnSurfaceCommand
    {
        public string SurfaceMaxCooridnates { get; set; }

        public IEnumerable<RobotInitializationViewModel> Robots { get; set; }

        public class Result
        {
            public IEnumerable<RobotResultViewModel> RobotsMovementResult { get; set; }
        }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(SurfaceMaxCooridnates))
                throw new ArgumentException("Invalid Surface max coordinates");
            if (!(Robots?.Any() ?? false))
                throw new ArgumentException("Invalid Robots info");
        }
    }

    public class RobotInitializationViewModel
    {
        public int OrderId { get; set; }

        public string StartPosition { get; set; }

        public string MovementCommands { get; set; }
    }

    public class RobotResultViewModel
    {
        public int Id { get; set; }

        public string Position { get; set; }
    }
}