﻿namespace MartianRobots.Application.Contracts.Enums
{
    public enum RobotOrientationViewEnum
    {
        North,
        South,
        East,
        West
    }
}