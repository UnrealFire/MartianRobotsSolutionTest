﻿namespace MartianRobots.Application.Contracts.Enums
{
    public enum RobotCommandViewEnum
    {
        Left,
        Right,
        Forward
    }
}