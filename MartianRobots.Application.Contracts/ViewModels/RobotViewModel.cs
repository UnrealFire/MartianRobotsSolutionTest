﻿using MartianRobots.Application.Contracts.Enums;

using System;

namespace MartianRobots.Application.Contracts.ViewModels
{
    public class RobotViewModel
    {
        // The constructor required by AutoMapper
        private RobotViewModel() { }

        public RobotViewModel(int id, SurfacePointViewModel startPoint, RobotOrientationViewEnum startOrientation)
        {
            Id = id;
            Position = startPoint;
            Orientation = startOrientation;
        }

        public int Id { get; private set; }

        public SurfacePointViewModel Position { get; private set; }

        public RobotOrientationViewEnum Orientation { get; private set; }

        public bool IsLost { get; set; }

        public override bool Equals(object obj)
        {
            return obj is RobotViewModel model &&
                   Id == model.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}