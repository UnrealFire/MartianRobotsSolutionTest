﻿namespace MartianRobots.Application.Contracts.ViewModels
{
    public class SurfacePointViewModel
    {
        // The constructor required by AutoMapper
        private SurfacePointViewModel() { }

        public SurfacePointViewModel(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }

        public int XCoordinate { get; private set; }

        public int YCoordinate { get; private set; }
    }
}
