﻿using AutoMapper;

using MartianRobots.Application.Contracts.Command;
using MartianRobots.Application.Contracts.Enums;
using MartianRobots.Application.Contracts.ViewModels;
using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Models;

using System;

namespace MartianRobots.Application.Mapping
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<RobotViewModel, RobotModel>()
                .ReverseMap();

            CreateMap<RobotCommandViewEnum, RobotCommandEnum>()
                .ReverseMap();

            CreateMap<RobotCommandViewEnum, RobotCommandEnum>()
                .ReverseMap();

            CreateMap<SurfacePointViewModel, SurfacePointModel>()
                .ReverseMap();

            CreateMap<RobotViewModel, RobotResultViewModel>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.Position, o => o.MapFrom(s => GetRobotPositionString(s)));
        }

        private string GetRobotPositionString(RobotViewModel robot)
        {
            var result = $"{robot.Position.XCoordinate} {robot.Position.YCoordinate} {ConvertOrientationToChar(robot.Orientation)}";

            if (robot.IsLost)
                result += " LOST";

            return result;
        }

        private char ConvertOrientationToChar(RobotOrientationViewEnum orientation)
        {
            switch (orientation)
            {
                case RobotOrientationViewEnum.North:
                    return 'N';
                case RobotOrientationViewEnum.South:
                    return 'S';
                case RobotOrientationViewEnum.East:
                    return 'E';
                case RobotOrientationViewEnum.West:
                    return 'W';
                default:
                    throw new ArgumentException("Invalid robot orientation");
            }
        }
    }
}