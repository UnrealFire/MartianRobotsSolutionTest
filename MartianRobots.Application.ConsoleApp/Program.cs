﻿using MartianRobots.Application.Contracts.Command;
using MartianRobots.Application.Handlers.Command;
using MartianRobots.Application.Infrastructure;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MartianRobots.Application.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var bootstrapper = new Bootstrapper();
            bootstrapper.ConfigureServices();
            var serviceProvider = bootstrapper.Build();

            Console.WriteLine("Welcome to the Martian Robots Test Console App!");

            var command = CreateRobotsCommandFromInput();

            var result = GetResultFromRobotCommand(serviceProvider, command);

            WriteRobotCommandResult(result);

            Console.WriteLine("Press any key to close the application...");
            Console.ReadKey();
        }

        private static RunRobotsOnSurfaceCommand.Result GetResultFromRobotCommand(IServiceProvider serviceProvider, RunRobotsOnSurfaceCommand command)
        {
            var commandHandler = serviceProvider.GetRequiredService<MartianRobotsCommandHandler>();
            return commandHandler.Handle(command);
        }

        private static RunRobotsOnSurfaceCommand CreateRobotsCommandFromInput()
        {
            var command = new RunRobotsOnSurfaceCommand();

            command.SurfaceMaxCooridnates = GetSurfaceCoordinatesFromInput();
            command.Robots = GetRobotsFromInput();

            return command;
        }

        private static string GetSurfaceCoordinatesFromInput()
        {
            Console.WriteLine($"Write Martian Surface max X and Y coordinates." +
                $"{Environment.NewLine}Sample Input: '5 3'" +
                $"{Environment.NewLine}Min coordinates: 0, 0");

            return Console.ReadLine();
        }

        private static IEnumerable<RobotInitializationViewModel> GetRobotsFromInput()
        {
            var robots = new List<RobotInitializationViewModel>();

            var continueInput = true;
            var count = 0;

            while (continueInput)
            {
                count++;
                robots.Add(GetRobotFromInput(count));

                Console.WriteLine("Write 'Y' to initialize next robot");
                var continueInputChar = Console.ReadLine().FirstOrDefault();
                continueInput = continueInputChar == 'Y' || continueInputChar == 'y';
            }

            return robots;
        }

        private static RobotInitializationViewModel GetRobotFromInput(int orderId)
        {
            Console.WriteLine($"Lets initialize robot {orderId}!");

            var robot = new RobotInitializationViewModel();
            robot.OrderId = orderId;

            robot.StartPosition = GetRobotPositionFromInput();
            robot.MovementCommands = GetRobotMovementCommandsFromInput();

            return robot;
        }

        private static string GetRobotPositionFromInput()
        {
            Console.WriteLine($"Write robot start coordinates and orientation." +
                $"{Environment.NewLine}Sample Input: '1 1 E', where first two numbers are coordinates and third symbol is orientation." +
                $"{Environment.NewLine}Orientations: N - North, S - South, E - East, W - West");

            return Console.ReadLine();
        }

        private static string GetRobotMovementCommandsFromInput()
        {
            Console.WriteLine($"Write robot movement commands." +
                $"{Environment.NewLine}Sample Input: 'RFRFRFRF', where every symbol is command" +
                $"{Environment.NewLine}Commands: R - Right Rotate, L - Left Rotate, F - Forward Movement");

            return Console.ReadLine();
        }

        private static void WriteRobotCommandResult(RunRobotsOnSurfaceCommand.Result result)
        {
            Console.WriteLine($"Robots movement results." +
                $"{Environment.NewLine}First two numers are last coordinates, third symbol is last orientation and last word LOST if the robot is lost");

            foreach (var robot in result.RobotsMovementResult)
            {
                Console.WriteLine($"Robot Id={robot.Id} result position: {robot.Position}");
            }
        }
    }
}