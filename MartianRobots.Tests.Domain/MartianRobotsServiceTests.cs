using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Models;
using MartianRobots.Domain.Services;

using System.Collections.Generic;

using Xunit;

namespace MartianRobots.Tests.Domain
{
    public class MartianRobotsServiceTests
    {
        protected readonly RobotModel _testRobotModel =
            new RobotModel(1, new SurfacePointModel(3, 2), RobotOrientationEnum.East);

        [Fact]
        public void InitializeContext_NotThrown()
        {
            GetServiceWithInitializedTestContext();
        }

        [Fact]
        public void GetRobots_Equal()
        {
            var service = GetServiceWithInitializedTestContext();

            var robot = service.GetRobot(_testRobotModel.Id);

            Assert.Equal(robot, _testRobotModel);
        }

        [Fact]
        public void GetRobots_NotEmpty()
        {
            var service = GetServiceWithInitializedTestContext();

            var robots = service.GetRobots();

            Assert.NotEmpty(robots);
        }

        [Fact]
        public void MoveRobot_NotThrown()
        {
            var service = GetServiceWithInitializedTestContext();
            var command = RobotCommandEnum.Forward;

            service.MoveRobot(_testRobotModel.Id, command);
        }

        private MartianRobotsService GetServiceWithInitializedTestContext()
        {
            var service = new MartianRobotsService();

            var xCoordinate = 5;
            var yCoordinate = 3;
            var robots = new List<RobotModel>() { _testRobotModel };

            service.InitializeContext(xCoordinate, yCoordinate, robots);

            return service;
        }
    }
}