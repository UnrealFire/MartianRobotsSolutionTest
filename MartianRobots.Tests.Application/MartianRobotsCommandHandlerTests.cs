using AutoMapper;

using MartianRobots.Application.Contracts.Command;
using MartianRobots.Application.Handlers.Command;
using MartianRobots.Application.Mapping;
using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Interfaces;
using MartianRobots.Domain.Contracts.Models;

using Moq;

using System.Collections.Generic;

using Xunit;

namespace MartianRobots.Tests.Application
{
    public class MartianRobotsCommandHandlerTests
    {
        protected readonly IMapper _mapper;

        protected readonly RobotModel _testRobotModel =
            new RobotModel(1, new SurfacePointModel(3, 2), RobotOrientationEnum.East);

        public MartianRobotsCommandHandlerTests()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddMaps(typeof(ApplicationMappingMarkerType)));
            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public void Handle_RunRobotsOnSurfaceCommand_NotNull()
        {
            var serviceMock = new Mock<IMartianRobotsService>();

            serviceMock.Setup(s => s.InitializeContext(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<List<RobotModel>>()));
            serviceMock.Setup(s => s.MoveRobot(It.IsAny<int>(), It.IsAny<RobotCommandEnum>()));
            serviceMock.Setup(s => s.GetRobot(It.IsAny<int>())).Returns(_testRobotModel);
            serviceMock.Setup(s => s.GetRobots()).Returns(new List<RobotModel>() { _testRobotModel });

            var handler = new MartianRobotsCommandHandler(_mapper, serviceMock.Object);

            var command = new RunRobotsOnSurfaceCommand
            {
                SurfaceMaxCooridnates = "5 5",

                Robots = new List<RobotInitializationViewModel>
                {
                    new RobotInitializationViewModel
                    {
                        OrderId = _testRobotModel.Id,
                        StartPosition = "0 0 E",
                        MovementCommands = "RFRF"
                    }
                }
            };

            var result = handler.Handle(command);

            Assert.NotNull(result);
        }
    }
}