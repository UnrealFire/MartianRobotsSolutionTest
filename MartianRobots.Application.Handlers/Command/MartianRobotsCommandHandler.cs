﻿using AutoMapper;

using MartianRobots.Application.Contracts.Command;
using MartianRobots.Application.Contracts.Enums;
using MartianRobots.Application.Contracts.ViewModels;
using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Interfaces;
using MartianRobots.Domain.Contracts.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MartianRobots.Application.Handlers.Command
{
    public class MartianRobotsCommandHandler
    {
        protected readonly IMapper _mapper;
        protected readonly IMartianRobotsService _service;

        public MartianRobotsCommandHandler(IMapper mapper, IMartianRobotsService service)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public RunRobotsOnSurfaceCommand.Result Handle(RunRobotsOnSurfaceCommand command)
        {
            command.Validate();

            (var xCoordinate, var yCoordinate) = ParseSurfaceMaxCoordinatesString(command.SurfaceMaxCooridnates);

            RunAllRobotsCommands(command, xCoordinate, yCoordinate);

            return GetRunRobotsCommandResult();
        }

        private void RunAllRobotsCommands(RunRobotsOnSurfaceCommand command, int xCoordinate, int yCoordinate)
        {
            var robotsWithMovementCommands = new Dictionary<RobotViewModel, IEnumerable<RobotCommandViewEnum>>();

            foreach (var robot in command.Robots)
            {
                (var robotViewModel, var movementCommands) = ParseRobotInitializationViewModel(robot);

                robotsWithMovementCommands.Add(robotViewModel, movementCommands);
            }

            var domainRobotModels = _mapper.Map<List<RobotModel>>(robotsWithMovementCommands.Keys.ToList());

            _service.InitializeContext(xCoordinate, yCoordinate, domainRobotModels);

            foreach (var robot in robotsWithMovementCommands)
            {
                RunSingleRobotCommands(robot.Key.Id, robot.Value);
            }
        }

        private void RunSingleRobotCommands(int id, IEnumerable<RobotCommandViewEnum> commands)
        {
            foreach (var command in commands)
            {
                var domainCommand = _mapper.Map<RobotCommandEnum>(command);

                _service.MoveRobot(id, domainCommand);
            }
        }

        private RunRobotsOnSurfaceCommand.Result GetRunRobotsCommandResult()
        {
            var domainRobots = _service.GetRobots();
            var robots = _mapper.Map<List<RobotViewModel>>(domainRobots);

            return new RunRobotsOnSurfaceCommand.Result
            {
                RobotsMovementResult = _mapper.Map<List<RobotResultViewModel>>(robots)
            };
        }

        private (int, int) ParseSurfaceMaxCoordinatesString(string surfaceMaxCoordinates)
        {
            var coordinates = surfaceMaxCoordinates.Split(' ');

            var parseXSuccessful = int.TryParse(coordinates[0], out var xCoordinate);
            var parseYSuccessful = int.TryParse(coordinates[1], out var yCoordinate);

            if (!(parseXSuccessful && parseYSuccessful))
                throw new ArgumentException("Invalid surface max coordinates");

            if (!(IsCoordinateValid(xCoordinate) && IsCoordinateValid(yCoordinate)))
                throw new ArgumentException("Invalid surface max coordinates");

            return (xCoordinate, yCoordinate);
        }

        private (RobotViewModel, IEnumerable<RobotCommandViewEnum>) ParseRobotInitializationViewModel(RobotInitializationViewModel robotInitialization)
        {
            (var positionModel, var orientation) = ParseRobotStartPositionString(robotInitialization.StartPosition);

            var robot = new RobotViewModel(robotInitialization.OrderId, positionModel, orientation);

            if (robotInitialization.MovementCommands.Length > 100)
                throw new ArgumentException($"Robot Id={robotInitialization.OrderId} length of commands must be less than 100");

            var commands = ParseRobotCommandString(robotInitialization.MovementCommands);

            return (robot, commands);
        }

        private (SurfacePointViewModel, RobotOrientationViewEnum) ParseRobotStartPositionString(string robotPosition)
        {
            var positionValues = robotPosition.Split(' ');

            var parseXSuccessful = int.TryParse(positionValues[0], out var xCoordinate);
            var parseYSuccessful = int.TryParse(positionValues[1], out var yCoordinate);
            var parseOrientationSuccessful = TryParseOrientationChar(positionValues[2].FirstOrDefault(), out var orientation);

            if (!(parseXSuccessful && parseYSuccessful && parseOrientationSuccessful))
                throw new ArgumentException("Invalid robot initialization data");

            if (!(IsCoordinateValid(xCoordinate) && IsCoordinateValid(yCoordinate)))
                throw new ArgumentException("Invalid robot start coordinates");

            return (new SurfacePointViewModel(xCoordinate, yCoordinate), orientation.Value);
        }

        private bool TryParseOrientationChar(char orientation, out RobotOrientationViewEnum? result)
        {
            result = null;

            switch (orientation)
            {
                case 'N':
                    result = RobotOrientationViewEnum.North;
                    return true;
                case 'S':
                    result = RobotOrientationViewEnum.South;
                    return true;
                case 'E':
                    result = RobotOrientationViewEnum.East;
                    return true;
                case 'W':
                    result = RobotOrientationViewEnum.West;
                    return true;
                default:
                    return false;
            }
        }

        private IEnumerable<RobotCommandViewEnum> ParseRobotCommandString(string commandsString)
        {
            var commands = new List<RobotCommandViewEnum>();
            foreach (var commandChar in commandsString)
            {
                var parseCommandSuccessful = TryParseCommandChar(commandChar, out var command);
                if (!parseCommandSuccessful)
                    throw new ArgumentException("Invalid robot initialization data");

                commands.Add(command.Value);
            }

            return commands;
        }

        private bool TryParseCommandChar(char orientation, out RobotCommandViewEnum? result)
        {
            result = null;

            switch (orientation)
            {
                case 'L':
                    result = RobotCommandViewEnum.Left;
                    return true;
                case 'R':
                    result = RobotCommandViewEnum.Right;
                    return true;
                case 'F':
                    result = RobotCommandViewEnum.Forward;
                    return true;
                default:
                    return false;
            }
        }

        private bool IsCoordinateValid(int coordinate)
        {
            return coordinate <= 50;
        }
    }
}