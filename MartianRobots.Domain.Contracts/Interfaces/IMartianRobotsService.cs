﻿using MartianRobots.Domain.Contracts.Enums;
using MartianRobots.Domain.Contracts.Models;

using System.Collections.Generic;

namespace MartianRobots.Domain.Contracts.Interfaces
{
    public interface IMartianRobotsService
    {
        void InitializeContext(int xMaxCoordinate, int yMaxCoordinate, IEnumerable<RobotModel> robots);

        void MoveRobot(int id, RobotCommandEnum command);

        RobotModel GetRobot(int id);

        IEnumerable<RobotModel> GetRobots();
    }
}