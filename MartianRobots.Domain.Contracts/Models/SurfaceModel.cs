﻿using System.Collections.Generic;

namespace MartianRobots.Domain.Contracts.Models
{
    public class SurfaceModel
    {
        public int XMinCoordinate { get; } = 0;

        public int YMinCoordinate { get; } = 0;

        // The constructor required by AutoMapper
        private SurfaceModel() { }

        public SurfaceModel(int xMaxCoordinate, int yMaxCoordinate)
        {
            XMaxCoordinate = xMaxCoordinate;
            YMaxCoordinate = yMaxCoordinate;

            RobotScentPoints = new List<SurfacePointModel>();
        }

        public int XMaxCoordinate { get; private set; }

        public int YMaxCoordinate { get; private set; }

        public List<SurfacePointModel> RobotScentPoints { get; private set; }
    }
}