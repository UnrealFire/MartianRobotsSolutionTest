﻿using MartianRobots.Domain.Contracts.Enums;

using System;

namespace MartianRobots.Domain.Contracts.Models
{
    public class RobotModel
    {
        // The constructor required by AutoMapper
        private RobotModel() { }

        public RobotModel(int id, SurfacePointModel startPoint, RobotOrientationEnum startOrientation)
        {
            Id = id;
            Position = startPoint;
            Orientation = startOrientation;
        }

        public int Id { get; private set; }

        public SurfacePointModel Position { get; private set; }

        public RobotOrientationEnum Orientation { get; private set; }

        public bool IsLost { get; set; }

        public void RotateLeft()
        {
            Orientation = CalculateRotateLeft();
        }

        public RobotOrientationEnum CalculateRotateLeft()
        {
            switch (Orientation)
            {
                case RobotOrientationEnum.North:
                    return RobotOrientationEnum.West;
                case RobotOrientationEnum.South:
                    return RobotOrientationEnum.East;
                case RobotOrientationEnum.East:
                    return RobotOrientationEnum.North;
                case RobotOrientationEnum.West:
                    return RobotOrientationEnum.South;
                default:
                    throw new Exception($"Unknown robot orientation");
            }
        }

        public void RotateRight()
        {
            Orientation = CalculateRotateRight();
        }

        public RobotOrientationEnum CalculateRotateRight()
        {
            switch (Orientation)
            {
                case RobotOrientationEnum.North:
                    return RobotOrientationEnum.East;
                case RobotOrientationEnum.South:
                    return RobotOrientationEnum.West;
                case RobotOrientationEnum.East:
                    return RobotOrientationEnum.South;
                case RobotOrientationEnum.West:
                    return RobotOrientationEnum.North;
                default:
                    throw new Exception($"Unknown robot orientation");
            }
        }

        public void MoveForward()
        {
            Position = CalculateMoveForward();
        }

        public SurfacePointModel CalculateMoveForward()
        {
            var calculatedPosition = new SurfacePointModel(Position);

            switch (Orientation)
            {
                case RobotOrientationEnum.North:
                    calculatedPosition.AddY();
                    break;
                case RobotOrientationEnum.South:
                    calculatedPosition.SubtractY();
                    break;
                case RobotOrientationEnum.East:
                    calculatedPosition.AddX();
                    break;
                case RobotOrientationEnum.West:
                    calculatedPosition.SubtractX();
                    break;
                default:
                    throw new Exception($"Unknown robot orientation");
            }

            return calculatedPosition;
        }

        public override bool Equals(object obj)
        {
            return obj is RobotModel model &&
                   Id == model.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}