﻿using System;

namespace MartianRobots.Domain.Contracts.Models
{
    public class SurfacePointModel
    {
        // The constructor required by AutoMapper
        private SurfacePointModel() { }

        public SurfacePointModel(SurfacePointModel point)
        {
            XCoordinate = point.XCoordinate;
            YCoordinate = point.YCoordinate;
        }

        public SurfacePointModel(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }

        public int XCoordinate { get; private set; }

        public int YCoordinate { get; private set; }

        public void AddX()
        {
            XCoordinate++;
        }

        public void SubtractX()
        {
            XCoordinate--;
        }

        public void AddY()
        {
            YCoordinate++;
        }

        public void SubtractY()
        {
            YCoordinate--;
        }

        public override bool Equals(object obj)
        {
            return obj is SurfacePointModel model &&
                   XCoordinate == model.XCoordinate &&
                   YCoordinate == model.YCoordinate;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(XCoordinate, YCoordinate);
        }
    }
}