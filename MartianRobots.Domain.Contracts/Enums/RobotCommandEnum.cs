﻿namespace MartianRobots.Domain.Contracts.Enums
{
    public enum RobotCommandEnum
    {
        Left,
        Right,
        Forward
    }
}