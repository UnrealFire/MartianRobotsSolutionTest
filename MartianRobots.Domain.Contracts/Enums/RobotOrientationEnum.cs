﻿namespace MartianRobots.Domain.Contracts.Enums
{
    public enum RobotOrientationEnum
    {
        North,
        South,
        East,
        West
    }
}